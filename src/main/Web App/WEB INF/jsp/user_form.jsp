<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
    <%@ taglib uri ="http://java.sun.com/jsp/jstl/core" prefix="c"%>
    <%@ taglib uri ="http://www.springframework.org/tags" prefix="spring"%> 
    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<div class="Container">
     <spring:url value="/user/saveUser" var="saveURL"/>
     <h2>User</h2>
     <form:form modelAttribute="userform" method="post" action="${saveURL}" cssClass="form">
     <form:hidden path="id"/>
     <div class="form-group">
     <label>Title</label>label>
     <form:input path="title" cssClass="form-control" id="title"/>
     </div>
     <div class="form-group">
       <label>Category</label>
       <form:input path="category" cssClass="form-control" id="category" />
     </div>
     <button type="submit"class="bin bin-primary">Save</button>
     </form:form>
     </div>
</body>
</html>
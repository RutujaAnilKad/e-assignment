package com.eassignment.apps;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EAssignmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(EAssignmentApplication.class, args);
	}

}


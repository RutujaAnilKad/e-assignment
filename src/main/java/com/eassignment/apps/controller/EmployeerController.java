package com.eassignment.apps.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.eassignment.apps.model.Employeer;
import com.eassignment.apps.service.EmployeerService;

@Controller
//@RequestMapping(value="/employeer",method=RequestMethod.GET)
public class EmployeerController {

	@Autowired
	EmployeerService employeerService;
	
	@RequestMapping(value="/addemployeer",method=RequestMethod.GET)	
	public ModelAndView addEmployeer() {
		ModelAndView model=new ModelAndView();
		Employeer employeer =new Employeer();
		model.addObject("employeerForm",employeer);
		model.setViewName("employeer_form");
		System.out.println("inside add employeer "+model);
		return model;
	}
}

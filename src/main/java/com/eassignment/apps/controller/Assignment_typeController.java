package com.eassignment.apps.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.eassignment.apps.model.Assignment_type;
import com.eassignment.apps.service.Assignment_typeService;

@Controller
//@RequestMapping(value="/assignment_type",method=RequestMethod.GET)
public class Assignment_typeController {

	@Autowired
	Assignment_typeService assignment_typeService;
	
	@RequestMapping(value="/addassignment_type",method=RequestMethod.GET)	
	public ModelAndView addAssignment_type() {
		ModelAndView model=new ModelAndView();
		Assignment_type assignment_type =new Assignment_type();
		model.addObject("assignmentForm",assignment_type);
		model.setViewName("assignment_type_form");
		System.out.println("inside add assignment_type "+model);
		return model;
       }
}	
	

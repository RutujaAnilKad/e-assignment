package com.eassignment.apps.controller;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.eassignment.apps.model.User;
import com.eassignment.apps.service.UserService;

@Controller
//@RequestMapping(value="/user",method=RequestMethod.GET)
public class UserController {
	
	 
	@Autowired
	UserService userService;
	
	@RequestMapping(value="/adduser",method=RequestMethod.GET)	
	public ModelAndView addUser() {
		ModelAndView model=new ModelAndView();
		User user =new User();
		model.addObject("userForm",user);
		model.setViewName("user_form");
		System.out.println("inside add user "+model);
		return model;
		
	}


}

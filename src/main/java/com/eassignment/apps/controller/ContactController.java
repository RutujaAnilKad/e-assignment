package com.eassignment.apps.controller;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;



@Controller
public class ContactController {

		@Value("${contact.text}")
	    private String text;
	    
	    
	    @GetMapping("/contact")
	    public String main(Model model) {
	        model.addAttribute("text", text);
	       

	        return "contact"; //view
	    }

}	

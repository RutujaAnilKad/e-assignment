package com.eassignment.apps.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eassignment.apps.model.Assignment;
import com.eassignment.apps.repository.AssignmentRepository;

@Service
@Transactional
public class AssignmentServiceImpl implements AssignmentService {

	@Autowired
	AssignmentRepository assignmentRepository;


	@Override
	public void add(Assignment assignment) {
		// TODO Auto-generated method stub
		assignmentRepository.save(assignment);
	} 
	
}

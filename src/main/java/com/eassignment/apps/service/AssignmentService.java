package com.eassignment.apps.service;

import com.eassignment.apps.model.Assignment;

public interface AssignmentService {
   public void add(Assignment assignment);
}

package com.eassignment.apps.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eassignment.apps.model.Assignment_type;
import com.eassignment.apps.repository.Assignment_typeRepository;

@Service
@Transactional
public class Assignment_typeServiceImpl implements Assignment_typeService {
	
	@Autowired
	Assignment_typeRepository assignment_typeRepository;

	@Override
	public void add(Assignment_type assignment_type) {
		// TODO Auto-generated method stub
		assignment_typeRepository.save(assignment_type);
	}
	
}

package com.eassignment.apps.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eassignment.apps.model.Employeer;
import com.eassignment.apps.repository.EmployeerRepository;

@Service
@Transactional
public class EmployeerServiceImpl implements EmployeerService{
	
	@Autowired
	EmployeerRepository employeerRepository;
	@Override
	public void add(Employeer employeer) {
		// TODO Auto-generated method stub
		employeerRepository.save(employeer);
	}
	
}

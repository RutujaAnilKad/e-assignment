package com.eassignment.apps.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Assignment_type {
	@Id 
	@GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="assignment_type_no")
	private int assignment_type_no;
	
    @Column(name="assignment_type_name")	
    private String assignment_type_name;
    
    @Column(name="assignment_type_description")
    private String assignment_type_description; 
    
    public Assignment_type() {
    	super();
   }
    
   public int getAssignment_type_no() {
	   return assignment_type_no;
   }
   public void setAssignment_type_no(int assignment_type_no) {
	    this.assignment_type_no = assignment_type_no;
   }
   public String getAssignment_type_name() {
	    return assignment_type_name;
   } 
   public void setAssignment_type_name(String assignment_type_name) {
	    this.assignment_type_name = assignment_type_name;
   }
   public String getAssignment_type_description() {
	    return assignment_type_description;
   }
   public void setAssignment_type_description(String assignment_type_description) {
	    this.assignment_type_description = assignment_type_description;
   }
   public Assignment_type(int assignment_type_no, String assignment_type_name, String assignment_type_description) {
	    super();
	    this.assignment_type_no = assignment_type_no;
	    this.assignment_type_name = assignment_type_name;
	    this.assignment_type_description = assignment_type_description;
   }

}
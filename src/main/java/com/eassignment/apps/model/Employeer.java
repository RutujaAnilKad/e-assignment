package com.eassignment.apps.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Employeer {
     @Id
     @GeneratedValue(strategy=GenerationType.AUTO)
     private int employeeID;
     @Column(name="lastname")
     private String lastname;
     
     @Column(name="firstname")
     private String fristname;
     
     @Column(name="phone_no")
     private int phone_no; 
     
     @Column(name="emailID")
     private String emailID;
  
     public Employeer() {
    	 super();
     }
     public int getEmployeeID() {
	      return employeeID;
     }
     public void setEmployeeID(int employeeID) {
	      this.employeeID = employeeID;
     }
     public String getLastname() {
	      return lastname;
     }
     public void setLastname(String lastname) {
	      this.lastname = lastname;
     }
     public String getFristname() {
	      return fristname;
     }
     public void setFristname(String fristname) {
	 this.fristname = fristname;
     }
     public int getPhone_no() {
	 return phone_no;
     }
     public void setPhone_no(int phone_no) {
	 this.phone_no = phone_no;
     }
     public String getEmailID() {
	 return emailID;
     }
     public void setEmailID(String emailID) {
	 this.emailID = emailID;
     }
     public Employeer(int employeeID, String lastname, String fristname, int phone_no, String emailID) {
	      super();
	      this.employeeID = employeeID;
	      this.lastname = lastname;
	      this.fristname = fristname;
	      this.phone_no = phone_no;
	      this.emailID = emailID;
     }

}
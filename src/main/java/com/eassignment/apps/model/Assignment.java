package com.eassignment.apps.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Assignment {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="assignment_no")
	private int assignment_no;
	
	@Column(name="assignment_name")
	private String assignment_name;
	
	@Column(name="assignment_description")
	private String assignment_description;
	
	public Assignment() {
		super();
	}
	public int getAssignment_no() {
		return assignment_no;
	}
	public void setAssignment_no(int assignment_no) {
		this.assignment_no = assignment_no;
	}
	public String getAssignment_name() {
		return assignment_name;
	}
	public void setAssignment_name(String assignment_name) {
		this.assignment_name = assignment_name;
	}
	public String getAssignment_description() {
		return assignment_description;
	}
	public void setAssignment_description(String assignment_description) {
		this.assignment_description = assignment_description;
	}
	public Assignment(int assignment_no, String assignment_name, String assignment_description) {
		super();
		this.assignment_no = assignment_no;
		this.assignment_name = assignment_name;
		this.assignment_description = assignment_description;
	}
	
}
package com.eassignment.apps.repository;

import org.springframework.data.repository.CrudRepository;

import com.eassignment.apps.model.Assignment_type;

public interface Assignment_typeRepository extends CrudRepository<Assignment_type,Long> {

}

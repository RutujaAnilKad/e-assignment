package com.eassignment.apps.repository;

import org.springframework.data.repository.CrudRepository;

import com.eassignment.apps.model.User;

public interface UserRepository extends CrudRepository<User,Long> {

}

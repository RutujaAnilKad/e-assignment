package com.eassignment.apps.repository;

import org.springframework.data.repository.CrudRepository;

import com.eassignment.apps.model.Assignment;

public interface AssignmentRepository extends CrudRepository<Assignment,Long> {

}

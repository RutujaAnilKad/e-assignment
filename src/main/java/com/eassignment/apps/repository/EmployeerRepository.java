package com.eassignment.apps.repository;

import org.springframework.data.repository.CrudRepository;

import com.eassignment.apps.model.Employeer;

public interface EmployeerRepository extends CrudRepository<Employeer,Long> {

}
